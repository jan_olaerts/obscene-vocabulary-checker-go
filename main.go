package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

var wordsToCheck []string

func main() {
	fileName := getStringFromUser()
	wordsToCheck = getFileContents(fileName)
	askSentenceAndCheckForTaboo()
}

func getStringFromUser() string {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	return scanner.Text()
}

func printFileContents(fileName string) {
	fileBytes, err := ioutil.ReadFile(fileName)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(string(fileBytes))
}

func getFileContents(fileName string) []string {
	fileBytes, err := ioutil.ReadFile(fileName)
	if err != nil {
		log.Fatal(err)
	}

	return strings.Split(string(fileBytes), "\n")
}

func askSentenceAndCheckForTaboo() {
	var sentence string

	for {
		sentence = getStringFromUser()
		if sentence == "exit" {
			fmt.Println("Bye!")
			return
		}
		printSentenceWithoutTabooWords(sentence)
		fmt.Println("")
	}
}

func printSentenceWithoutTabooWords(sentence string) {
	words := strings.Split(sentence, " ")
	for index, word := range words {
		if containsIgnoreCase(wordsToCheck, word) {
			printStarStringWithOrWithoutSpace(word, index, words)
			continue
		}
		printStringWithOrWithoutSpace(word, index, words)
	}
}

func printStarStringWithOrWithoutSpace(str string, index int, words []string) {
	if isLastIndex(index, words) {
		fmt.Print(createStarString(str))
	} else {
		fmt.Print(createStarString(str) + " ")
	}
}

func printStringWithOrWithoutSpace(str string, index int, words []string) {
	if isLastIndex(index, words) {
		fmt.Print(str)
	} else {
		fmt.Print(str + " ")
	}
}

func isLastIndex(index int, sa []string) bool {
	return index == len(sa)-1
}

func containsIgnoreCase(slice []string, str string) bool {

	for _, value := range slice {
		if strings.TrimSpace(strings.ToLower(value)) == strings.TrimSpace(strings.ToLower(str)) {
			return true
		}
	}

	return false
}

func createStarString(word string) string {

	var starString string
	for range word {
		starString += "*"
	}

	return starString
}
